/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./src/index.ts":
/*!**********************!*\
  !*** ./src/index.ts ***!
  \**********************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\r\nObject.defineProperty(exports, \"__esModule\", ({ value: true }));\r\nconst users_1 = __webpack_require__(/*! ./users */ \"./src/users.ts\");\r\nconst userInfo_1 = __webpack_require__(/*! ./userInfo */ \"./src/userInfo.ts\");\r\nconst getUsersJobPositions = (usersArray) => {\r\n    const resultUsersArray = [];\r\n    usersArray.forEach((user) => {\r\n        userInfo_1.usersInfoArray.forEach((userInfo) => {\r\n            user.userid === userInfo.userid && resultUsersArray.push(new users_1.UserWithPosition(user, userInfo));\r\n        });\r\n    });\r\n    return resultUsersArray;\r\n};\r\nconst usersPositions = getUsersJobPositions(users_1.usersArray);\r\nconsole.log(usersPositions);\r\n\n\n//# sourceURL=webpack://homework-ts1/./src/index.ts?");

/***/ }),

/***/ "./src/userInfo.ts":
/*!*************************!*\
  !*** ./src/userInfo.ts ***!
  \*************************/
/***/ ((__unused_webpack_module, exports) => {

eval("\r\nObject.defineProperty(exports, \"__esModule\", ({ value: true }));\r\nexports.usersInfoArray = void 0;\r\nexports.usersInfoArray = [\r\n    {\r\n        userid: '127e4567-e89b-12d3-a458-426614174000',\r\n        name: 'John',\r\n        birthdate: '1982-02-17T21:00:00.000Z',\r\n        age: 40,\r\n        organization: {\r\n            name: 'Amazon',\r\n            position: 'General manager'\r\n        }\r\n    },\r\n    {\r\n        userid: '127e4567-e89a-12f3-a458-327395154000',\r\n        name: 'Anna',\r\n        birthdate: '1988-02-17T21:00:00.000Z',\r\n        age: 34,\r\n        organization: {\r\n            name: 'Amazon',\r\n            position: 'Manager'\r\n        }\r\n    }\r\n];\r\n\n\n//# sourceURL=webpack://homework-ts1/./src/userInfo.ts?");

/***/ }),

/***/ "./src/users.ts":
/*!**********************!*\
  !*** ./src/users.ts ***!
  \**********************/
/***/ ((__unused_webpack_module, exports) => {

eval("\r\nObject.defineProperty(exports, \"__esModule\", ({ value: true }));\r\nexports.UserWithPosition = exports.usersArray = void 0;\r\nexports.usersArray = [\r\n    {\r\n        userid: '127e4567-e89b-12d3-a458-426614174000',\r\n        name: 'John',\r\n        gender: 'man'\r\n    },\r\n    {\r\n        userid: '127e4567-e89a-12f3-a458-327395154000',\r\n        name: 'Anna',\r\n        gender: 'woman'\r\n    }\r\n];\r\nclass UserWithPosition {\r\n    constructor(user, userInfo) {\r\n        this.name = user.name;\r\n        this.position = userInfo.organization.position;\r\n        this.age = userInfo.age;\r\n        this.gender = user.gender;\r\n    }\r\n}\r\nexports.UserWithPosition = UserWithPosition;\r\n\n\n//# sourceURL=webpack://homework-ts1/./src/users.ts?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = __webpack_require__("./src/index.ts");
/******/ 	
/******/ })()
;