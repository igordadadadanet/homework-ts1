export interface User {
    userid: string,
    name: string,
    gender: string
}

export interface Organization {
    name: string,
    position: string
}

export interface UserInfo {
    userid: string,
    name: string,
    birthdate: string,
    age: number,
    organization: Organization
}

export interface UserPosition {
    name: string,
    position: string,
    age: number,
    gender: string
}