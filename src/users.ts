import { User, UserInfo, UserPosition } from "./types/types";

export const usersArray: User[] = [
    {
        userid: '127e4567-e89b-12d3-a458-426614174000',
        name: 'John',
        gender: 'man'
    },
    {
        userid: '127e4567-e89a-12f3-a458-327395154000',
        name: 'Anna',
        gender: 'woman'
    }

]

export class UserWithPosition implements UserPosition {
    name: string;
    position: string;
    age: number;
    gender: string;

    constructor(user: User, userInfo: UserInfo) {
        this.name = user.name;
        this.position = userInfo.organization.position;
        this.age = userInfo.age;
        this.gender = user.gender;
    }
}