import { usersArray, UserWithPosition } from "./users";
import { User, UserInfo, UserPosition } from "./types/types";
import { usersInfoArray } from "./userInfo";

const getUsersJobPositions = (usersArray: User[]): UserPosition[] => {
    const resultUsersArray: UserPosition[] = [];
    usersArray.forEach((user: User) => {
        usersInfoArray.forEach((userInfo: UserInfo) => {
            user.userid === userInfo.userid && resultUsersArray.push(new UserWithPosition(user, userInfo));
        })
    })
    return resultUsersArray;
}

const usersPositions = getUsersJobPositions(usersArray);

console.log(usersPositions)